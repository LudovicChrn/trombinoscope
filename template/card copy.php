<?php 
require("./models/getData.php");
?>

<div class="">
    <h2>Nombres d'apprenants: <?php echo count($data); ?></h2>
</div>

<div class="search-filter">
    <form action="">

    <select name="" id="">
        <option value="">-- Choisir Promotion --</option>
    </select>

    <input type="text" placeholder="Rechercher un apprenant..">

    <select name="" id="">
        <option value="">-- Choisir une compétence --</option>
    </select>

    </form>
</div>


<div class="trombinoscope">
        <?php 
            foreach ($data as $value) {
                $skill = '';

                if(!empty($value->competences[0]->name) && !empty($value->competences[1]->name)){
                    $skill = $value->competences[0]->name;
                    $skill2 = $value->competences[1]->name;
                }
        ?>
        <div class="c26-card">

            <div class="top-content">

            <div class="card-name-shape"></div>
            <div class="title-card">
                <div class="title-name">
                    <p class="card-surname"><?php echo $value->nom;?></p>
                    <p class="card-name"><?php echo $value->prenom;?></p>
                </div>

                <p class="card-promo">
                <?php  
                    echo substr($value->promotion->name, 0, 1) . substr($value->promotion->name, 10);
                ?>
                </p>
            </div>
            
            <div class="picture-card">
                <img src="<?php echo $value->featured_media?>" class="card-picture" alt="">
            </div>

            <div class="extrait-card">
                
                <p class="card-extrait"><?php echo $value->yoast_head_json->og_description; ?></p>
            </div>

            </div>

            <div class="bottom-content">
                <div class="competence-card">
                    <p class="card-competence">
                        <?php 
                        echo $skill . " " . $skill2; 
                        ?>
                    </p>
                </div>

                <div class="links-card">
                    <ul class="card-links">
                        <li><a class="portfolio" target="_blank" href="<?php echo $value->portfolio; ?>"><i class="fas fa-book-open"></i></a></li>
                        <li><a class="linkedin" target="_blank" href="<?php echo $value->linkedin; ?>"><i class="fab fa-linkedin"></i></a></li>
                        <li><a class="cv" target="_blank" href="<?php echo $value->cv;?>"><i class="fas fa-download"></i></a></li>
                </div>
            </div>

            
        </div>
        <?php 
        }
        ?>
</div>