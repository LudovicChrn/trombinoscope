<?php

    class ApprenticeFetcher {

        public $apiC26;

        public function __construct($apiC26) 
        {
            $this->apiC26 = $apiC26;
        }

        public function getApprenticeData() {
            $data = $this->fetchData();
            return $this->decode($data);
        }

        private function fetchData() {
            $curl = curl_init($this->apiC26);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($curl);
            if(!$data) {
                throw new Error("Oh non pas l'api");
            }

            return $data;
        }

        private function decode($jsonString) {
            return json_decode($jsonString);
        }

    }

    // $getName = new ApprenticeFetcher("http://localhost/wordpress_2/wp-json/wp/v2/apprenant?per_page=100");
    // $data = $getName->getApprenticeData();


    $getName = new ApprenticeFetcher('https://www.lecoledunumerique.fr/wp-json/wp/v2/apprenants?per_page=100');
    $data = $getName->getApprenticeData();
