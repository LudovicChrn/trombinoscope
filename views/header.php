<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css2?family=Racing+Sans+One&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/8334dc67da.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <header>
            <div class="header">

            </div>
            <div class="header-two">
                <div class="header-two-content">
                    <h1>Le trombinoscope</h1>
                    <h3>Des apprenants de l'école du numérique.</h3>
                </div>

                <div class="header-two-overlay"></div>
                <div class="header-two-shape"></div>
            </div>
        </header>